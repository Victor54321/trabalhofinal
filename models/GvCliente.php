<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gv_cliente".
 *
 * @property int $id
 * @property string $nome
 * @property string $email
 * @property string $senha
 *
 * @property GvCompra[] $gvCompras
 */
class GvCliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gv_cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'email', 'senha'], 'required'],
            [['nome', 'email'], 'string', 'max' => 100],
            [['senha'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'email' => 'Email',
            'senha' => 'Produto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGvCompras()
    {
        return $this->hasMany(GvCompra::className(), ['cliente' => 'id']);
    }
}
