<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gv_vendedor".
 *
 * @property int $id
 * @property string $nome
 * @property string $loja
 * @property string $email
 * @property string $senha
 *
 * @property GvProduto[] $gvProdutos
 */
class GvVendedor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gv_vendedor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'loja', 'email', 'senha'], 'required'],
            [['nome', 'loja', 'email'], 'string', 'max' => 100],
            [['senha'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'loja' => 'Loja',
            'email' => 'Email',
            'senha' => 'Senha',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGvProdutos()
    {
        return $this->hasMany(GvProduto::className(), ['vendedor' => 'id']);
    }
}
