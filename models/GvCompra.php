<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gv_compra".
 *
 * @property int $id
 * @property string $data
 * @property double $valor
 * @property int $cliente
 *
 * @property CompProd[] $compProds
 * @property GvProduto[] $produtos
 * @property GvCliente $cliente0
 */
class GvCompra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gv_compra';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data'], 'safe'],
            [['valor'], 'required'],
            [['valor'], 'number'],
            [['cliente'], 'integer'],
            [['cliente'], 'exist', 'skipOnError' => true, 'targetClass' => GvCliente::className(), 'targetAttribute' => ['cliente' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data' => 'Data',
            'valor' => 'Valor',
            'cliente' => 'Cliente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompProds()
    {
        return $this->hasMany(CompProd::className(), ['compra' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProdutos()
    {
        return $this->hasMany(GvProduto::className(), ['id' => 'produto'])->viaTable('comp_prod', ['compra' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCliente0()
    {
        return $this->hasOne(GvCliente::className(), ['id' => 'cliente']);
    }
}
