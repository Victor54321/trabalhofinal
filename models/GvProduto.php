<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gv_produto".
 *
 * @property int $id
 * @property string $nome
 * @property double $preco
 * @property string $descricao
 * @property int $vendedor
 *
 * @property CompProd[] $compProds
 * @property GvCompra[] $compras
 * @property GvVendedor $vendedor0
 */
class GvProduto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gv_produto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nome', 'preco', 'descricao'], 'required'],
            [['preco'], 'number'],
            [['descricao'], 'string'],
            [['vendedor'], 'integer'],
            [['nome'], 'string', 'max' => 100],
            [['vendedor'], 'exist', 'skipOnError' => true, 'targetClass' => GvVendedor::className(), 'targetAttribute' => ['vendedor' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nome' => 'Nome',
            'preco' => 'Preco',
            'descricao' => 'Descricao',
            'vendedor' => 'Vendedor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompProds()
    {
        return $this->hasMany(CompProd::className(), ['produto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(GvCompra::className(), ['id' => 'compra'])->viaTable('comp_prod', ['produto' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVendedor()
    {
        return $this->hasOne(GvVendedor::className(), ['id' => 'vendedor']);
    }
}
