<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\GvProduto;

/**
 * GvProdutoSearch represents the model behind the search form of `app\models\GvProduto`.
 */
class GvProdutoSearch extends GvProduto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nome', 'descricao','vendedor.nome'], 'safe'],
            [['preco'], 'number'],
        ];
    }


    public function attributes()
    {
        return array_merge(parent::attributes(), ['vendedor.nome']);
    }




    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GvProduto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->joinWith(['vendedor']);
        $dataProvider->sort->attributes['vendedor.nome']=[
            'asc'=>['vendedor.nome'=>SORT_ASC],
            'desc'=>['vendedor.nome'=>SORT_DESC],
        ];
     


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'preco' => $this->preco,
            'vendedor' => $this->vendedor,
        ]);

        $query->andFilterWhere(['like', 'nome', $this->nome])
            ->andFilterWhere(['like', 'descricao', $this->descricao]);


        $query->andFilterWhere(['LIKE', 'vendedor.nome',$this->getAttribute('vendedor.nome')]);

        return $dataProvider;
    }
}
