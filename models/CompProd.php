<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comp_prod".
 *
 * @property int $compra
 * @property int $produto
 *
 * @property GvCompra $compra0
 * @property GvProduto $produto0
 */
class CompProd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comp_prod';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['compra', 'produto'], 'required'],
            [['compra', 'produto'], 'integer'],
            [['compra', 'produto'], 'unique', 'targetAttribute' => ['compra', 'produto']],
            [['compra'], 'exist', 'skipOnError' => true, 'targetClass' => GvCompra::className(), 'targetAttribute' => ['compra' => 'id']],
            [['produto'], 'exist', 'skipOnError' => true, 'targetClass' => GvProduto::className(), 'targetAttribute' => ['produto' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'compra' => 'Compra',
            'produto' => 'Produto',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompra0()
    {
        return $this->hasOne(GvCompra::className(), ['id' => 'compra']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduto0()
    {
        return $this->hasOne(GvProduto::className(), ['id' => 'produto']);
    }
}
