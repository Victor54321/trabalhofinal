<?php

namespace app\models;

use Yii;
use app\models\MeCarona;
use app\models\MeCaronaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MeCaronaController implements the CRUD actions for MeCarona model.
 */
class MeCaronaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all MeCarona models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MeCaronaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single MeCarona model.
     * @param integer $usuario
     * @param integer $carona
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($usuario, $carona)
    {
        return $this->render('view', [
            'model' => $this->findModel($usuario, $carona),
        ]);
    }

    /**
     * Creates a new MeCarona model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MeCarona();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'usuario' => $model->usuario, 'carona' => $model->carona]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MeCarona model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $usuario
     * @param integer $carona
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($usuario, $carona)
    {
        $model = $this->findModel($usuario, $carona);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'usuario' => $model->usuario, 'carona' => $model->carona]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MeCarona model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $usuario
     * @param integer $carona
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($usuario, $carona)
    {
        $this->findModel($usuario, $carona)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MeCarona model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $usuario
     * @param integer $carona
     * @return MeCarona the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($usuario, $carona)
    {
        if (($model = MeCarona::findOne(['usuario' => $usuario, 'carona' => $carona])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
