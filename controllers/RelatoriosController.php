<?php
 
namespace app\controllers;
use Yii;
use yii\data\SqlDataProvider;

class RelatoriosController extends \yii\web\Controller
{
   public function actionIndex()
   {
       return $this->render('index');
   }
 
   public function actionRelatorio1()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT gv_vendedor.nomev, gv_produto.nome, gv_produto.preco
        FROM gv_vendedor, gv_produto
        WHERE gv_vendedor.id = gv_produto.vendedor
        ',
            ]
        );
        
        return $this->render('relatorio1', ['resultado' => $consulta]);
   }
   public function actionRelatorio2()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT count(nomev)
        FROM gv_vendedor
        ',
            ]
        );
        
        return $this->render('relatorio2', ['resultado' => $consulta]);
   }
   public function actionRelatorio3()
   {
       $consulta = new SqlDataProvider([
        'sql' => 'SELECT count(nome)
        FROM gv_produto
        ',
            ]
        );
        
        return $this->render('relatorio3', ['resultado' => $consulta]);
   }



}

