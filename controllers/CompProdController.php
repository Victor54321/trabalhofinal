<?php

namespace app\controllers;

use Yii;
use app\models\CompProd;
use app\models\CompProdSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CompProdController implements the CRUD actions for CompProd model.
 */
class CompProdController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CompProd models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CompProdSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CompProd model.
     * @param integer $compra
     * @param integer $produto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($compra, $produto)
    {
        return $this->render('view', [
            'model' => $this->findModel($compra, $produto),
        ]);
    }

    /**
     * Creates a new CompProd model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CompProd();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'compra' => $model->compra, 'produto' => $model->produto]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CompProd model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $compra
     * @param integer $produto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($compra, $produto)
    {
        $model = $this->findModel($compra, $produto);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'compra' => $model->compra, 'produto' => $model->produto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CompProd model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $compra
     * @param integer $produto
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($compra, $produto)
    {
        $this->findModel($compra, $produto)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CompProd model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $compra
     * @param integer $produto
     * @return CompProd the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($compra, $produto)
    {
        if (($model = CompProd::findOne(['compra' => $compra, 'produto' => $produto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
