<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MeCarona */

$this->title = 'Update Me Carona: ' . $model->usuario;
$this->params['breadcrumbs'][] = ['label' => 'Me Caronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario, 'url' => ['view', 'usuario' => $model->usuario, 'carona' => $model->carona]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="me-carona-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
