<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GvCliente */

$this->title = 'Create Gv Cliente';
$this->params['breadcrumbs'][] = ['label' => 'Gv Clientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gv-cliente-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
