<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MePontoscarona */

$this->title = 'Create Me Pontoscarona';
$this->params['breadcrumbs'][] = ['label' => 'Me Pontoscaronas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="me-pontoscarona-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
