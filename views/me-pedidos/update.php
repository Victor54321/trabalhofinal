<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MePedidos */

$this->title = 'Update Me Pedidos: ' . $model->usuario;
$this->params['breadcrumbs'][] = ['label' => 'Me Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->usuario, 'url' => ['view', 'usuario' => $model->usuario, 'carona' => $model->carona]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="me-pedidos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
