<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\MePedidos */

$this->title = $model->usuario;
$this->params['breadcrumbs'][] = ['label' => 'Me Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="me-pedidos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'usuario' => $model->usuario, 'carona' => $model->carona], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'usuario' => $model->usuario, 'carona' => $model->carona], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'usuario',
            'carona',
        ],
    ]) ?>

</div>
