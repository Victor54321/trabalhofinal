<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MePedidos */

$this->title = 'Create Me Pedidos';
$this->params['breadcrumbs'][] = ['label' => 'Me Pedidos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="me-pedidos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
