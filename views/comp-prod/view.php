<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CompProd */

$this->title = $model->compra;
$this->params['breadcrumbs'][] = ['label' => 'Comp Prods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="comp-prod-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('atualizar', ['update', 'compra' => $model->compra, 'produto' => $model->produto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Apagar', ['delete', 'compra' => $model->compra, 'produto' => $model->produto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'compra',
            'produto',
        ],
    ]) ?>

</div>
