<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CompProd */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comp-prod-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'compra')->textInput() ?>

    <?= $form->field($model, 'produto')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('salvar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
