<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CompProdSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comp Prods';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comp-prod-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Comp Prod', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'compra',
            'produto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
