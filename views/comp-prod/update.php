<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompProd */

$this->title = 'Update Comp Prod: ' . $model->compra;
$this->params['breadcrumbs'][] = ['label' => 'Comp Prods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->compra, 'url' => ['view', 'compra' => $model->compra, 'produto' => $model->produto]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="comp-prod-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
