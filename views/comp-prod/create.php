<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CompProd */

$this->title = 'Create Comp Prod';
$this->params['breadcrumbs'][] = ['label' => 'Comp Prods', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comp-prod-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
