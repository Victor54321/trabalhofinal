<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GvProduto */

$this->title = 'Novo Produto';
$this->params['breadcrumbs'][] = ['label' => 'Gv Produtos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gv-produto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
