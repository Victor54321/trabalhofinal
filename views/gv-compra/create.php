<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GvCompra */

$this->title = 'Create Gv Compra';
$this->params['breadcrumbs'][] = ['label' => 'Gv Compras', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gv-compra-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
