<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\GvVendedor */

$this->title = 'Novo Vendedor';
$this->params['breadcrumbs'][] = ['label' => 'Gv Vendedors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gv-vendedor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
